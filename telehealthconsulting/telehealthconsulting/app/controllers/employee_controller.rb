# frozen_string_literal: true

# Class for Employee Controller
class EmployeeController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
