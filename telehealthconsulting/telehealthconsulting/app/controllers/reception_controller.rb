# frozen_string_literal: true

# Class for Reception Controller
class ReceptionController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
