# frozen_string_literal: true

# Class for Admin Controller
class AdminController < ApplicationController
  before_action :authenticate_user!
  def index; end

  def show; end

  def new
    @user = User.new
  end

  def add_user
    user_params = params[:user].permit(:email, :password, :password_confirmation, :role_id)

    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_index_path, notice: 'User created successfully' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
