# frozen_string_literal: true

# Class for Volunteer Controller
class VolunteerController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
