# frozen_string_literal: true

# Class for Medical Officer controller
class MedicalOfficerController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
