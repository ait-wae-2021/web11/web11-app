# frozen_string_literal: true

# Class for Team Leader Controller
class TeamLeaderController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
