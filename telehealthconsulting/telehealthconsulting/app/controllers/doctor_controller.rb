# frozen_string_literal: true

class DoctorController < ApplicationController
  before_action :authenticate_user!
  def index; end
end
