# frozen_string_literal: true

# Class for Application Controller
class ApplicationController < ActionController::Base
  def after_sign_in_path_for(_resource)
    session_index_path(current_user)
  end
end
