Feature: User
  The application needs to maintain the information about Users

  Scenario: Admin Sign in

    Given I am an admin user
    When I sign in
    Then I should see admin dashboard
    When I sign out
    Then I should see login page

  Scenario: Doctor Sign in

    Given I am a doctor
    When I sign in
    Then I should see doctor dashboard
    When I sign out
    Then I should see login page

  Scenario: Reception Sign in

    Given I am a reception
    When I sign in
    Then I should see reception dashboard
    When I sign out
    Then I should see login page

  Scenario: Team Leader Sign in

    Given I am a team leader
    When I sign in
    Then I should see team leader dashboard
    When I sign out
    Then I should see login page

  Scenario: Medical Officer Sign in

    Given I am a medical officer
    When I sign in
    Then I should see medical officer dashboard
    When I sign out
    Then I should see login page

  Scenario: Employee Sign in

    Given I am an employee
    When I sign in
    Then I should see employee dashboard
    When I sign out
    Then I should see login page

  Scenario: Volunteer Sign in

    Given I am a volunteer
    When I sign in
    Then I should see volunteer dashboard
    When I sign out
    Then I should see login page