# frozen_string_literal: true

FactoryBot.define do
  factory :admin_role, class: Role do
    name { 'Admin' }
    code { '0' }
  end

  factory :admin, class: User do
    email { 'admin@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:admin_role) }
  end

  factory :doctor_role, class: Role do
    name { 'Doctor' }
    code { '1' }
  end

  factory :doctor, class: User do
    email { 'doctor@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:doctor_role) }
  end

  factory :reception_role, class: Role do
    name { 'Reception' }
    code { '2' }
  end

  factory :reception, class: User do
    email { 'reception@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:reception_role) }
  end

  factory :teamleader_role, class: Role do
    name { 'Team Leader' }
    code { '3' }
  end

  factory :teamleader, class: User do
    email { 'teamleader@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:teamleader_role) }
  end

  factory :medicalofficer_role, class: Role do
    name { 'Medical Officer' }
    code { '4' }
  end

  factory :medicalofficer, class: User do
    email { 'medicalofficer@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:medicalofficer_role) }
  end

  factory :employee_role, class: Role do
    name { 'Employee' }
    code { '5' }
  end

  factory :employee, class: User do
    email { 'employee@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:employee_role) }
  end

  factory :volunteer_role, class: Role do
    name { 'Volunteer' }
    code { '6' }
  end

  factory :volunteer, class: User do
    email { 'volunteer@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:volunteer_role) }
  end
end
