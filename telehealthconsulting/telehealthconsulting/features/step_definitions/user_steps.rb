# frozen_string_literal: true

Given('I am an admin user') do
  @user = FactoryBot.create(:admin)
end

When('I sign in') do
  visit root_path
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
end

Then('I should see admin dashboard') do
  expect(page).to have_content 'Admin'
end

When('I sign out') do
  find_link('Sign out', href: '/users/sign_out').click
end

Then('I should see login page') do
  expect(page).to have_content 'Log in'
end

Given('I am a doctor') do
  @user = FactoryBot.create(:doctor)
end

Then('I should see doctor dashboard') do
  expect(page).to have_content 'Doctor'
end

Given('I am a reception') do
  @user = FactoryBot.create(:reception)
end

Then('I should see reception dashboard') do
  expect(page).to have_content 'Reception'
end

Given('I am a team leader') do
  @user = FactoryBot.create(:teamleader)
end

Then('I should see team leader dashboard') do
  expect(page).to have_content 'TeamLeader'
end

Given('I am a medical officer') do
  @user = FactoryBot.create(:medicalofficer)
end

Then('I should see medical officer dashboard') do
  expect(page).to have_content 'MedicalOfficer'
end

Given('I am an employee') do
  @user = FactoryBot.create(:employee)
end

Then('I should see employee dashboard') do
  expect(page).to have_content 'Employee'
end

Given('I am a volunteer') do
  @user = FactoryBot.create(:volunteer)
end

Then('I should see volunteer dashboard') do
  expect(page).to have_content 'volunteer'
end
