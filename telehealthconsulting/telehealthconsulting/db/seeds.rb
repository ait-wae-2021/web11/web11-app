# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
@admin = Role.create(name: 'Admin', code: '0')
User.create(
  email: 'admin@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @admin
)

@doctor = Role.create(name: 'Doctor', code: '1')
User.create(
  email: 'doctor@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @doctor
)

@reception = Role.create(name: 'Reception', code: '2')
User.create(
  email: 'reception@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @reception
)

@teamleader = Role.create(name: 'Team Leader', code: '3')
User.create(
  email: 'teeamleader@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @teamleader
)

@medicalofficer = Role.create(name: 'Medical Officer', code: '4')
User.create(
  email: 'medicalofficer@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @medicalofficer
)

@employee = Role.create(name: 'Employee', code: '5')
User.create(
  email: 'employee@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @employee
)

@volunteer = Role.create(name: 'Volunteer', code: '6')
User.create(
  email: 'volunteer@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  role: @volunteer
)
