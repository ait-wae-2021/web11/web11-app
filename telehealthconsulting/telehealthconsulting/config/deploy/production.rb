# frozen_string_literal: true

server 'telehealthconsultmm.com', user: 'deploy', roles: %w[app db web]

set :deploy_to, '/home/deploy/telehealthconsulting'

set :yarn_flags, '--production --silent --no-progress --network-timeout 1000000'
