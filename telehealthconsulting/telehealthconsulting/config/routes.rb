# frozen_string_literal: true

Rails.application.routes.draw do
  get 'volunteer/index'
  get 'employee/index'
  get 'medical_officer/index'
  get 'team_leader/index'
  get 'reception/index'
  get 'doctor/index'
  get 'admin/index'
  get 'admin/add_user', to: 'admin#new'
  post 'admin/add_user', to: 'admin#add_user'
  get 'session/index'
  devise_for :users, skip: [:registrations]
  as :user do
    get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end
  devise_scope :user do
    root to: 'devise/sessions#new'
  end
end
