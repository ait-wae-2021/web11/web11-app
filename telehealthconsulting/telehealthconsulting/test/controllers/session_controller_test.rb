# frozen_string_literal: true

require 'test_helper'

class SessionControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index for admin' do
    sign_in users(:one)
    get session_index_url
    assert_redirected_to admin_index_url
  end

  test 'should get index for doctor' do
    sign_in users(:two)
    get session_index_url
    assert_redirected_to doctor_index_url
  end

  test 'should get index for reception' do
    sign_in users(:three)
    get session_index_url
    assert_redirected_to reception_index_url
  end

  test 'should get index for team leader' do
    sign_in users(:four)
    get session_index_url
    assert_redirected_to team_leader_index_path
  end

  test 'should get index for medical officer' do
    sign_in users(:five)
    get session_index_url
    assert_redirected_to medical_officer_index_path
  end

  test 'should get index for employee' do
    sign_in users(:six)
    get session_index_url
    assert_redirected_to employee_index_path
  end

  test 'should get index for volunteer' do
    sign_in users(:seven)
    get session_index_url
    assert_redirected_to volunteer_index_path
  end
end
