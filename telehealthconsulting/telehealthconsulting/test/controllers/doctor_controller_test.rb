# frozen_string_literal: true

require 'test_helper'

class DoctorControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:two)
    get doctor_index_url
    assert_response :success
  end
end
