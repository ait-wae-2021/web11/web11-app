# frozen_string_literal: true

require 'test_helper'

class EmployeeControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:six)
    get employee_index_url
    assert_response :success
  end
end
