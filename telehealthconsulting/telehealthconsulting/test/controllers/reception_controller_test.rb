# frozen_string_literal: true

require 'test_helper'

class ReceptionControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:three)
    get reception_index_url
    assert_response :success
  end
end
