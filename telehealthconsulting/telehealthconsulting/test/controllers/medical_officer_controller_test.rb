# frozen_string_literal: true

require 'test_helper'

class MedicalOfficerControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:five)
    get medical_officer_index_url
    assert_response :success
  end
end
