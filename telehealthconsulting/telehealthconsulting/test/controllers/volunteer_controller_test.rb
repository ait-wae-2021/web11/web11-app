# frozen_string_literal: true

require 'test_helper'

class VolunteerControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:seven)
    get volunteer_index_url
    assert_response :success
  end
end
