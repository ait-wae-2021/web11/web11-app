# frozen_string_literal: true

# require 'faker'
# require 'database_cleaner'
#
# DatabaseCleaner.clean_with(:truncation)

@admin_role = Role.create(name: 'Admin', code: '0')
@doctor_role = Role.create(name: 'Doctor', code: '1')
@reception_role = Role.create(name: 'Reception', code: '2')
@teamleader_role = Role.create(name: 'Team Leader', code: '3')
@medicalofficer_role = Role.create(name: 'Medical Officer', code: '4')
# @employee_role = Role.create(name: 'Employee', code: '5')
# @volunteer_role = Role.create(name: 'Volunteer', code: '6')

@female = Gender.create(gender: 'Female', code: 0)
@male = Gender.create(gender: 'Male', code: 1)
@other = Gender.create(gender: 'Other', code: 2)

@admin = User.create(
  name: 'Admin Name',
  marital_status: true,
  education: 'Bachelor of Computer Technology (B.C.Tech)',
  address: 'Myanmar',
  email: 'admin@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  is_banned: false,
  team: 1,
  role: @admin_role,
  gender: @male
)

Phone.create(number: '09123456789', mobile: true, user: @admin)
Phone.create(number: '01123456', mobile: false, user: @admin)

@doctor = User.create(
  name: 'Doctor Name',
  marital_status: true,
  education: 'M.B.B.S',
  address: 'Myanmar',
  email: 'doctor@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  is_banned: false,
  team: 1,
  role: @doctor_role,
  gender: @female
)

Phone.create(number: '09123456789', mobile: true, user: @doctor)
Phone.create(number: '01123456', mobile: false, user: @doctor)

@reception = User.create(
  name: 'Reception Name',
  marital_status: true,
  education: 'B.C.Sc',
  address: 'Myanmar',
  email: 'reception@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  is_banned: false,
  team: 1,
  role: @reception_role,
  gender: @female
)

Phone.create(number: '09123456789', mobile: true, user: @reception)
Phone.create(number: '01123456', mobile: false, user: @reception)

@teamleader = User.create(
  name: 'Team Leader Name',
  marital_status: true,
  education: 'M.B.A',
  address: 'Myanmar',
  email: 'teamleader@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  is_banned: false,
  team: 1,
  role: @teamleader_role,
  gender: @male
)

Phone.create(number: '09123456789', mobile: true, user: @teamleader)
Phone.create(number: '01123456', mobile: false, user: @teamleader)

@medicalofficer = User.create(
  name: 'Medical Officer Name',
  marital_status: true,
  education: 'M.B.B.A',
  address: 'Myanmar',
  email: 'medicalofficer@teleconsult.com',
  password: 'password',
  password_confirmation: 'password',
  is_banned: false,
  team: 1,
  role: @medicalofficer_role,
  gender: @male
)

Phone.create(number: '09123456789', mobile: true, user: @medicalofficer)
Phone.create(number: '01123456', mobile: false, user: @medicalofficer)

# @employee = User.create(
#   name: Faker::Name.name,
#   marital_status: true,
#   education: 'B.C.Sc',
#   address: Faker::Address.full_address,
#   email: 'empployee@teleconsult.com',
#   password: 'password',
#   password_confirmation: 'password',
#   is_banned: false,
#   team: 1,
#   role: @employee_role,
#   gender: @male
# )
#
# Phone.create(number: '09123456789', mobile: true, user: @employee)
# Phone.create(number: '01123456', mobile: false, user: @employee)
#
# @volunteer = User.create(
#   name: Faker::Name.name,
#   marital_status: true,
#   education: 'B.C.Sc',
#   address: Faker::Address.full_address,
#   email: 'volunteer@teleconsult.com',
#   password: 'password',
#   password_confirmation: 'password',
#   is_banned: false,
#   team: 1,
#   role: @volunteer_role,
#   gender: @male
# )
#
# Phone.create(number: '09123456789', mobile: true, user: @volunteer)
# Phone.create(number: '01123456', mobile: false, user: @volunteer)
