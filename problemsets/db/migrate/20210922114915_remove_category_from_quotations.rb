class RemoveCategoryFromQuotations < ActiveRecord::Migration[6.1]
  def change
    remove_column :quotations, :category, :string
  end
end
