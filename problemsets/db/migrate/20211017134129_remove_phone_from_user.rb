class RemovePhoneFromUser < ActiveRecord::Migration[6.1]
  def change
    remove_reference :users, :phone, null: false, foreign_key: true
  end
end
