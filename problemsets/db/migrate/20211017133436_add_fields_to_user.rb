class AddFieldsToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :name, :string
    add_reference :users, :gender, null: false, foreign_key: true
    add_column :users, :marital_status, :boolean
    add_column :users, :education, :string
    add_column :users, :address, :text
    add_column :users, :team, :integer
  end
end
