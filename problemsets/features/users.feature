Feature: User
  The application needs to maintain the information about Users

  Scenario: Admin Sign in

    Given I am an admin user
    When I sign in as admin
    Then I should see admin dashboard
    When I sign out
    Then I should see login page

  Scenario: Doctor Sign in

    Given I am a doctor
    When I sign in as doctor
    Then I should see doctor dashboard
    When I sign out
    Then I should see login page

  Scenario: Reception Sign in

    Given I am a reception
    When I sign in as reception
    Then I should see reception dashboard
    When I sign out
    Then I should see login page

  Scenario: Team Leader Sign in

    Given I am a team leader
    When I sign in as team leader
    Then I should see team leader dashboard
    When I sign out
    Then I should see login page

  Scenario: Medical Officer Sign in

    Given I am a medical officer
    When I sign in as medical officer
    Then I should see medical officer dashboard
    When I sign out
    Then I should see login page