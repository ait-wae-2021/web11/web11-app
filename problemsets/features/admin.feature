Feature: Admin
  The admin can create, edit, delete, ban and allow the user in admin dashboard

  Scenario: Add New User
    Given I am an admin user
    And I want to create new user
    And I am signed in
    When I visit the admin dashboard
    Then I should see the link to add user
    When I click the link to add user
    Then I should see a form to add user
    When I submit the form
    Then I should see the list of users
    And I should see the user added

  Scenario: Edit user
    Given I am an admin user
    And I am signed in
    Given I have user to edit
    When I visit the admin dashboard
    Then I should see the link to edit user
    When I click the link to edit user
    Then I should see a form to enter user details
    And I enter the user details
    When I update the form
    Then I should see the list of users
    And I should see the user edited

  Scenario: Delete the user
    Given I am an admin user
    And I am signed in
    Given I have user to delete
    When I visit the admin dashboard
    Then I should see the list of users
    And I should see the link to delete user
    When I click the link to delete user
    Then I should not see the deleted user

  Scenario: Banned the user
    Given I am an admin user
    And I am signed in
    Given I have user to ban
    When I visit the admin dashboard
    Then I should see the list of users
    And I should see the link to ban user
    When I click the link to ban user
    Then the banned user should not able to login

  Scenario: Allow the user
    Given I am an admin user
    And I am signed in
    Given I have user to allow
    When I visit the admin dashboard
    Then I should see the list of users
    And I should see the link to allow user
    When I click the link to allow user
    Then the banned user should  able to login