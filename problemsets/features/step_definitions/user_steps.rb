# frozen_string_literal: true

Given('I am an admin user') do
  @admin = FactoryBot.create(:admin)
end

When('I sign in as admin') do
  visit new_user_session_path
  fill_in 'Email', with: @admin.email
  fill_in 'Password', with: @admin.password
  click_button 'Log in'
end

Then('I should see admin dashboard') do
  expect(page).to have_content 'Admin'
end

When('I sign out') do
  find_link('Sign out', href: '/users/sign_out').click
end

Then('I should see login page') do
  find_link('Problem Set 4: More odds and ends and user management', href: '/ps4/index').click
  find_link('User Management System', href: new_user_session_url).click
  expect(page).to have_content 'Log In'
end

Given('I am a doctor') do
  @doctor = FactoryBot.create(:doctor)
end

When('I sign in as doctor') do
  visit new_user_session_path
  fill_in 'Email', with: @doctor.email
  fill_in 'Password', with: @doctor.password
  click_button 'Log in'
end

Then('I should see doctor dashboard') do
  expect(page).to have_content 'Doctor'
end

Given('I am a reception') do
  @reception = FactoryBot.create(:reception)
end

When('I sign in as reception') do
  visit new_user_session_path
  fill_in 'Email', with: @reception.email
  fill_in 'Password', with: @reception.password
  click_button 'Log in'
end

Then('I should see reception dashboard') do
  expect(page).to have_content 'Reception'
end

Given('I am a team leader') do
  @teamleader = FactoryBot.create(:teamleader)
end

When('I sign in as team leader') do
  visit new_user_session_path
  fill_in 'Email', with: @teamleader.email
  fill_in 'Password', with: @teamleader.password
  click_button 'Log in'
end

Then('I should see team leader dashboard') do
  expect(page).to have_content 'TeamLeader'
end

Given('I am a medical officer') do
  @medicalofficer = FactoryBot.create(:medicalofficer)
end

When('I sign in as medical officer') do
  visit new_user_session_path
  fill_in 'Email', with: @medicalofficer.email
  fill_in 'Password', with: @medicalofficer.password
  click_button 'Log in'
end

Then('I should see medical officer dashboard') do
  expect(page).to have_content 'MedicalOfficer'
end
