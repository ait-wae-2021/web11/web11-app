# <-------------------------------------------------- Add Quote -------------------------------------------------->


Given('I am visiting the web page for problem set {int} assignment') do |int|
  visit basic2_quotations_path
end

When('I enter the fields in the form for adding new Quote') do
  fill_in 'Author name', with: 'Edward R. Murrow'
  select 'New', from: "quotation_category_id"
  fill_in 'categories[category]', with: 'Technology'
  fill_in 'Quote', with: "The newest computer can merely compound, at speed, the oldest problem in the relations between human beings, and in the end the communicator will be confronted with the old problem, of what to say and how to say it."

end

When('I click {string}') do |string|
  click_button 'Create'
end

Then('I should be able to see new quote added to the database') do
  expect(page).to have_content 'Edward R. Murrow'
  expect(page).to have_content 'Technology'
  expect(page).to have_content 'The newest computer can merely compound, at speed, the oldest problem in the relations between human beings, and in the end the communicator will be confronted with the old problem, of what to say and how to say it.'
end

# <-------------------------------------------------- Kill Quote -------------------------------------------------->

Given('I have added a quote in problem set 2 web page') do
  visit basic2_quotations_path
  # fill_in 'Author name', with: 'Frank Leahy'
  # select 'New', from: "quotation_category_id"
  # fill_in 'categories[category]', with: 'Egotism'
  # fill_in 'Quote', with: "Egotism is the anesthetic that dulls the pain of stupidity."
end

Then('I should see the added quote') do
  expect(page).to have_content 'Frank Leahy'
  expect(page).to have_content 'Egotism'
  expect(page).to have_content 'Egotism is the anesthetic that dulls the pain of stupidity.'
end

When('I click Kill for that quote') do
  find(:xpath, "//tr[td[contains(.,'Frank Leahy')]]/td/a", :text => 'Kill').click
end

Then('I expect the quote not to show up in the web page') do
  expect(page).not_to have_content 'Frank Leahy'
  expect(page).not_to have_content 'Egotism'
  expect(page).not_to have_content 'Egotism is the anesthetic that dulls the pain of stupidity.'
end

When('I click Reset Kills') do
  click_link 'Reset Kills'
end

Then('I should be able to see that quote again') do
  expect(page).to have_content 'Frank Leahy'
  expect(page).to have_content 'Egotism'
  expect(page).to have_content 'Egotism is the anesthetic that dulls the pain of stupidity.'
end

# <-------------------------------------------------- Verify Links to SQL Session -------------------------------------------------->


Then('I should see the links to my SQL session') do
  expect(page).to have_content 'Link to text file transcript of SQL session'
  expect(page).to have_content 'Link to stock file'
end

Then('the appropriate href attributes should be there in those links') do
  expect(page).to have_link('Link to text file transcript of SQL session', href: '/pdf/problem_set_2_final.pdf')
  expect(page).to have_link('Link to stock file', href: '/pdf/stock.pdf')
end

# <-------------------------------------------------- Download Quote in XML and JSON format -------------------------------------------------->

Given('I enter a quote in Import and Export Quotations Section') do
  fill_in 'file_data', with: 'Some for renown, on scraps of learning dote, And think they grow immortal as they quote.'
end

When('I click Export to XML') do
  click_link 'Export to XML'
end

Then('I should receive a download file in xml format') do
  page.response_headers['Content-Disposition'].should include("filename=\"quotations.xml\"")
end

When('I click Export to JSON') do
  visit basic2_quotations_path
  click_link 'Export to JSON'
end

Then('I should receive a download file in json format') do
  page.response_headers['Content-Disposition'].should include("filename=\"quotations.json\"")
end
