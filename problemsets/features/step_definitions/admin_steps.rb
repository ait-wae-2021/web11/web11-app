# frozen_string_literal: true

Given('I want to create new user') do
  @user = FactoryBot.build(:admin_add)
end

Given('I am signed in') do
  visit new_user_session_path
  fill_in 'Email', with: @admin.email
  fill_in 'Password', with: @admin.password
  click_button 'Log in'
end

When('I visit the admin dashboard') do
  visit admin_index_url
end

Then('I should see the link to add user') do
  find_link('Add User', href: 'add_user')
end

When('I click the link to add user') do
  find_link('Add User', href: 'add_user').click
end

Then('I should see a form to add user') do
  expect(page).to have_text('Email')
  expect(page).to have_text('Password')
  expect(page).to have_text('Password confirmation')
  expect(page).to have_text('Role')
  expect(page).to have_button('Create')
end

When('I submit the form') do
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  fill_in 'Password confirmation', with: @user.password_confirmation
  find("#user_role_id option[value='#{Role.first.id}']").select_option
  click_button 'Create'
end

Then('I should see the list of users') do
  expect(page).to have_text('Admin')
  expect(page).to have_table('users_table')
end

Then('I should see the user added') do
  expect(page).to have_text(@user.email)
end

Given('I have user to edit') do
  @user = FactoryBot.create(:admin_add)
end

Then('I should see the link to edit user') do
  expect(page).to have_text('Admin')
  find_link('Edit', href: "/admin/edit_user?id=#{@user.id}")
end

When('I click the link to edit user') do
  find_link('Edit', href: "/admin/edit_user?id=#{@user.id}").click
end

Then('I should see a form to enter user details') do
  expect(page).to have_text('Edit')
  expect(page).to have_text('Email')
  expect(page).to have_text('Role')
  expect(page).to have_button('Update')
end

Then('I enter the user details') do
  find("#user_role_id option[value='#{Role.last.id}']").select_option
end

When('I update the form') do
  click_button 'Update'
end

Then('I should see the user edited') do
  expect(page).to have_text(@user.email)
end

Given('I have user to delete') do
  @user = FactoryBot.create(:admin_add)
end

Then('I should see the link to delete user') do
  find_link('Delete', href: "/admin/delete?id=#{@user.id}")
end

When('I click the link to delete user') do
  find_link('Delete', href: "/admin/delete?id=#{@user.id}").click
end

Then('I should not see the deleted user') do
  expect(page).not_to have_text(@user.email)
end

Given('I have user to ban') do
  @user = FactoryBot.create(:admin_add)
end

Then('I should see the link to ban user') do
  find_link('Ban', href: "/admin/ban_user?id=#{@user.id}")
end

When('I click the link to ban user') do
  find_link('Ban', href: "/admin/ban_user?id=#{@user.id}").click
end

Then('the banned user should not able to login') do
  find_link('Sign out', href: '/users/sign_out').click
  visit new_user_session_path
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
  expect(page).to have_text('Sign in')
end

Given('I have user to allow') do
  @user = FactoryBot.create(:admin_add)
  @user.is_banned = true
  @user.save
end

Then('I should see the link to allow user') do
  find_link('Allow', href: "/admin/allow_user?id=#{@user.id}")
end

When('I click the link to allow user') do
  find_link('Allow', href: "/admin/allow_user?id=#{@user.id}").click
end

Then('the banned user should  able to login') do
  find_link('Sign out', href: '/users/sign_out').click
  visit new_user_session_path
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
  expect(page).to have_text('Sign out')
end
