Given('I am checking out the web page for problem set {int} assignment') do |int|
  visit basics_path
end

Then('I should see headlines from New York Times and Bangkok Post') do
  expect(page).to have_content 'Headlines from New York Times'
end

Then('Divide by Zero Button') do
  expect(page).to have_content 'Divide By Zero'
end
