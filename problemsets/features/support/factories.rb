# frozen_string_literal: true

FactoryBot.define do
  factory :female, class: Gender do
    gender { 'Female' }
    code { 0 }
  end
  factory :male, class: Gender do
    gender { 'Male' }
    code { 1 }
  end
end

FactoryBot.define do
  factory :admin_role, class: Role do
    name { 'Admin' }
    code { '0' }
  end

  factory :admin, class: User do
    name { 'Test Admin Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'admin@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:admin_role) }
    gender { FactoryBot.create(:male) }
  end
end

FactoryBot.define do
  factory :doctor_role, class: Role do
    name { 'Doctor' }
    code { '1' }
  end

  factory :doctor, class: User do
    name { 'Test Doctor Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'doctor@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:doctor_role) }
    gender { FactoryBot.create(:female) }
  end
end

FactoryBot.define do
  factory :reception_role, class: Role do
    name { 'Reception' }
    code { '2' }
  end

  factory :reception, class: User do
    name { 'Test Reception Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'reception@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:reception_role) }
    gender { FactoryBot.create(:male) }
  end
end

FactoryBot.define do
  factory :teamleader_role, class: Role do
    name { 'Team Leader' }
    code { '3' }
  end

  factory :teamleader, class: User do
    name { 'Test Team Leader Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'teamleader@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:teamleader_role) }
    gender { FactoryBot.create(:female) }
  end
end

FactoryBot.define do
  factory :medicalofficer_role, class: Role do
    name { 'Medical Officer' }
    code { '4' }
  end

  factory :medicalofficer, class: User do
    name { 'Test Medical Officer Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'medicalofficer@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:medicalofficer_role) }
    gender { FactoryBot.create(:female) }
  end
end

FactoryBot.define do
  factory :admin_add, class: User do
    name { 'Test add Admin Name' }
    marital_status { true }
    education { 'Degree' }
    address { 'Myanmar' }
    email { 'testaddadmin@gmail.com' }
    password { 'password' }
    password_confirmation { 'password' }
    role { FactoryBot.create(:admin_role) }
    gender { FactoryBot.create(:male) }
  end
end
