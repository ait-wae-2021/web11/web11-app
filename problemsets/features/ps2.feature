
Feature: Ps2 Rule

  This is to test ps2 web page

  Scenario: Add Quote

    As a viewer, I should be able to add a new quote to the database

    Given I am visiting the web page for problem set 2 assignment
    When I enter the fields in the form for adding new Quote
    And I click 'Create'
    Then I should be able to see new quote added to the database

  Scenario: Kill Quote
    AS a viewer, I should be able to kill quotes and should be able reset them

    Given I have added a quote in problem set 2 web page
    Then I should see the added quote
    When I click Kill for that quote
    Then I expect the quote not to show up in the web page
    When I click Reset Kills
    Then I should be able to see that quote again

  Scenario:
  As a viewer, I should be able to see links to my SQL session

    Given I am visiting the web page for problem set 2 assignment
    Then I should see the links to my SQL session
    And the appropriate href attributes should be there in those links

  Scenario:
  As a viewer, I should eb able to download quotes in xml and json format
    Given I am visiting the web page for problem set 2 assignment
    And I enter a quote in Import and Export Quotations Section
    When I click Export to XML
    Then I should receive a download file in xml format
    When I click Export to JSON
    Then I should receive a download file in json format
