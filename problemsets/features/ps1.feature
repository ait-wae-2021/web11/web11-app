
Feature: Ps1 Rule

  This is to test 'Problem Set 1: Basics' web page

  Scenario: Check headlines and check divide by zero error

    As a viewer, I should be able to add a new quote to the database

    Given I am checking out the web page for problem set 1 assignment
    Then I should see headlines from New York Times and Bangkok Post
    And Divide by Zero Button
