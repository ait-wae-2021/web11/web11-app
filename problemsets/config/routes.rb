# frozen_string_literal: true

Rails.application.routes.draw do
  get 'ps6/index'
  get 'ps5/index'
  get 'team_leader/index'
  get 'team_leader/show'
  get 'team_leader/assign_team', to: 'team_leader#assign'
  put 'team_leader/assign_team', to: 'team_leader#update'
  get 'reception/index'
  get 'reception/show'
  get 'medical_officer/index'
  get 'medical_officer/show'
  get 'doctor/index'
  get 'doctor/show'
  get 'admin/index'
  get 'admin/add_user', to: 'admin#new'
  post 'admin/add_user', to: 'admin#add_user'
  get 'admin/edit_user', to: 'admin#edit'
  post 'admin/reset_pwd', to: 'admin#reset'
  put 'admin/edit_user', to: 'admin#update'
  post 'admin/delete', to: 'admin#destroy'
  post 'admin/ban_user', to: 'admin#ban'
  post 'admin/allow_user', to: 'admin#allow'
  get 'admin/show'
  get 'session/index'
  get 'ps4/index'
  devise_for :users
  get 'ps3/index'
  get 'ps3/reception'
  get 'ps3/medical_officer'
  get 'ps3/doctor'
  get 'ps3/team_leader'
  get 'basic2/quotations'
  post 'basic2/quotations'
  resources :basics do
    member do
      get :divide_by_zero
    end
  end
  # get 'basics/divide_by_zero'
  get 'home/index'
  get 'home/about'
  root to: 'home#index'

  # devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
