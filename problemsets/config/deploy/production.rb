raise "Could not find BAZOOKA_USER environment variable. Please set it in your .bashrc!" unless ENV['BAZOOKA_USER']
set :default_env, { 'http_proxy' => '192.41.170.23:3128',
                   'https_proxy' => '192.41.170.23:3128',
                   'BAZOOKA_USER' => 'st122246' }

server "web11", user: "deploy", roles: %w{app db web}

set :deploy_to, "/home/deploy/problemsets"

set :yarn_flags, '--production --silent --no-progress --network-timeout 1000000'
