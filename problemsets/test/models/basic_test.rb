require "test_helper"

class BasicTest < ActiveSupport::TestCase
  test 'Basic is valid' do
      assert basics(:one).valid?
      assert basics(:two).valid?
  end
end
