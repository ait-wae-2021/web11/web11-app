require "test_helper"
require 'capybara/rails'
require 'capybara/minitest'

class BasicsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  # Make `assert_*` methods behave like Minitest assertions
  include Capybara::Minitest::Assertions

  # Reset sessions and driver between tests
  teardown do
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  setup do
    @basic = basics(:one)
  end

  test "should get index" do
    get basics_url
    assert_response :success
  end

  test "should get new" do
    get new_basic_url
    assert_response :success
  end

  test "should create basic" do
    assert_difference('Basic.count') do
      post basics_url, params: { basic: { headline: @basic.headline, link: @basic.link } }
    end

    assert_redirected_to basic_url(Basic.last)
  end

  test "should show basic" do
    get basic_url(@basic)
    assert_response :success
  end

  test "should get edit" do
    get edit_basic_url(@basic)
    assert_response :success
  end

  test "should update basic" do
    patch basic_url(@basic), params: { basic: { headline: @basic.headline, link: @basic.link } }
    assert_redirected_to basic_url(@basic)
  end

  test "should destroy basic" do
    assert_difference('Basic.count', -1) do
      delete basic_url(@basic)
    end

    assert_redirected_to basics_url
  end

  test "should raise error for divide by zero" do
    visit basics_path
    find_link("Divide By Zero", :visible => :all).visible?
    assert_raises(ZeroDivisionError) do
      click_link("Divide By Zero")
    end
  end
end
