# frozen_string_literal: true

require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = User.find_by_email('admin@teleconsult.com')
  end

  test 'should get index' do
    sign_in users(:one)
    get admin_index_url
    assert_response :success
  end

  test 'should get new' do
    sign_in users(:one)
    get admin_add_user_url
    assert_response :success
  end

  test 'should create user' do
    sign_in users(:one)
    assert_difference('User.count') do
      post admin_add_user_path, params: { user: {
        name: 'Test admin',
        marital_status: true,
        gender_id: Gender.find_by_gender('Male').id,
        education: 'Education',
        address: 'Myanmar',
        team: 1,
        role_id: Role.find_by_name('Admin').id,
        email: 'testadmin@teleconsult.com',
        password: 'password',
        password_confirmation: 'password'
      } }
    end
    assert_redirected_to admin_index_url
  end

  test 'should fail when create user' do
    sign_in users(:one)
    assert_difference('User.count', 0) do
      post admin_add_user_url, params: { user: {
        name: 'Test admin',
        marital_status: nil,
        gender_id: 1,
        education: nil,
        address: nil,
        team: nil,
        role_id: 1,
        email: nil,
        password: nil,
        password_confirmation: nil
      } }
    end
    assert_response :unprocessable_entity
  end

  test 'should get edit' do
    sign_in users(:one)
    get admin_edit_user_url(id: @user.id)
    assert_response :success
  end

  test 'should update project' do
    sign_in users(:one)
    put admin_edit_user_url(id: @user.id), params: { user: {
      name: 'Test admin',
      marital_status: false,
      gender_id: Gender.find_by_gender('Male').id,
      education: 'B.C.Sc',
      address: 'Myanmar',
      team: 2,
      role_id: Role.find_by_name('Admin').id,
      email: 'testadmin@teleconsult.com'
    } }
    assert_redirected_to admin_index_url
  end

  test 'should fail when update project' do
    sign_in users(:one)
    put admin_edit_user_url(id: @user.id), params: { user: {
      name: 'Test admin',
      marital_status: nil,
      gender_id: 1,
      education: nil,
      address: nil,
      team: nil,
      role_id: 2,
      email: nil
    } }
    assert_response :unprocessable_entity
  end

  test 'should delete the user' do
    sign_in users(:one)
    post admin_delete_path(id: @user.id)
    assert_redirected_to admin_index_path
  end

  test 'should ban the user' do
    sign_in users(:one)
    post admin_ban_user_path(id: @user.id)
    assert_redirected_to admin_index_path
  end

  test 'should allow the user' do
    sign_in users(:one)
    post admin_allow_user_path(id: @user.id)
    assert_redirected_to admin_index_path
  end
end
