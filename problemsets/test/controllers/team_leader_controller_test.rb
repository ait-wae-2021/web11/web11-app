# frozen_string_literal: true

require 'test_helper'

class TeamLeaderControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  test 'should get index' do
    sign_in users(:four)
    get team_leader_index_url
    assert_response :success
  end
end
