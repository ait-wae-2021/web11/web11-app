require "test_helper"
require 'capybara/rails'
require 'capybara/rails'
require 'capybara/minitest'

class Basic2ControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  # Make `assert_*` methods behave like Minitest assertions
  include Capybara::Minitest::Assertions

  # Reset sessions and driver between tests
  teardown do
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  setup do
    @category = categories(:one)
    @quotation = quotations(:one)
  end

  test "should get quotations" do
    get basic2_quotations_url
    assert_response :success
  end

  test "should add quote" do
    visit basic2_quotations_path
    fill_in 'Author name', with: 'Frank Leahy'
    select 'New', from: "quotation_category_id"
    fill_in 'categories[category]', with: 'Egotism'
    fill_in 'Quote', with: "Egotism is the anesthetic that dulls the pain of stupidity."
    click_button 'Create'

    fill_in 'Author name', with: 'Edward Young'
    select 'Egotism', from: "quotation_category_id"
    fill_in 'Quote', with: "Some for renown, on scraps of learning dote, And think they grow immortal as they quote."
    click_button 'Create'
  end

  test "should fail to add new quote" do
    visit basic2_quotations_path
    # fill_in 'Author name', with: 'Edward Young'
    # select 'Egotism', from: "quotation_category_id"
    # fill_in 'Quote', with: "Some for renown, on scraps of learning dote, And think they grow immortal as they quote."
    click_button 'Create'
  end

  test "should kill quote" do
    visit basic2_quotations_path
    find(:xpath, "//tr[td[contains(.,'Frank Leahy')]]/td/a", :text => 'Kill').click
  end

  test "should reset quote kills" do
    visit basic2_quotations_path
    click_link 'Reset Kills'
  end

  test "should sort quote by date and category" do
    visit basic2_quotations_path
    find_link('Sort by date', :visible => :all).visible?
    click_link 'Sort by date'
    find_link('Sort by category', :visible => :all).visible?
    click_link 'Sort by category'
  end


end
