require 'rails_helper'

RSpec.describe "Receptions", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/reception/index"
      expect(response).to have_http_status(:success)
    end
  end

end
