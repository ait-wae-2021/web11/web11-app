require 'rails_helper'

RSpec.describe "MedicalOfficers", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/medical_officer/index"
      expect(response).to have_http_status(:success)
    end
  end

end
