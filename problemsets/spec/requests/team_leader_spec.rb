require 'rails_helper'

RSpec.describe "TeamLeaders", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/team_leader/index"
      expect(response).to have_http_status(:success)
    end
  end

end
