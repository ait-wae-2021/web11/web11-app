json.extract! basic, :id, :headline, :link, :created_at, :updated_at
json.url basic_url(basic, format: :json)
