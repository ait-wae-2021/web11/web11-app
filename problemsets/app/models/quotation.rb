class Quotation < ApplicationRecord
  belongs_to :category

  validates_presence_of :author_name, :quote, :category_id
end
