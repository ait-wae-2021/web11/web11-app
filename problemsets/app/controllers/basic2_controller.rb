# frozen_string_literal: true

# Controller for Problem set 2
class Basic2Controller < ApplicationController
  require 'nokogiri'

  def quotations
    @category2 = Category.all

    if params[:categories]
      @category = Category.new(category_params)
      @category.save
    end

    if params[:quotation]
      @quotation = Quotation.new
      @quotation.author_name = quotation_params[:author_name]
      @quotation.quote = quotation_params[:quote]
      if params[:categories]
        @quotation.category_id = @category.id
      else
        @quotation.category_id = quotation_params[:category_id]
      end

      if @quotation.save
        flash[:notice] = 'Quotation was successfully created'
        @quotation = Quotation.new
      else
        @quotation.errors.full_messages.each do |message|
          puts message
        end
      end
    else
      @quotation = Quotation.new
    end

    # Check Cookies reset
    if params[:reset]
      if reset_params[:value]
        cookies[:ids] = nil
        reset_params[:value] = nil
      end
    end

    # Check the killed quote
    if params[:quotation_id]
      if cookies[:ids].nil?
        cookies[:ids] = ''
      end
      id_string = cookies[:ids] + ' ' +  kill_quote_params[:id]
      id_string = id_string.split(' ').uniq.join(' ')
      cookies[:ids] = id_string
    end

    # Search function
    if params[:search]
      if cookies[:ids].nil?
        search_query = "lower(quote) LIKE ? OR lower(author_name) LIKE ?"
      else
        search_query = "(lower(quote) LIKE ? OR lower(author_name) LIKE ?) AND (id NOT IN (#{cookies[:ids].split(' ').join(',')}))"
      end
      @quotations = Quotation.where(search_query, "%#{params[:search].downcase}%", "%#{params[:search].downcase}%")
    else
      if cookies[:ids] == '' || cookies[:ids].nil?
        @quotations = Quotation.all
      else
        sort_query = "(id NOT IN (#{cookies[:ids].split(' ').join(',')}))"
        @quotations = Quotation.where(sort_query)
      end

      if params[:sort_by] == 'date'
        @quotations = @quotations.order(:created_at)
      else
        @quotations = @quotations.order(:category_id)
      end
    end

    if params[:file_data]
      file_data = params[:file_data]
      doc = Nokogiri::XML(file_data)
      doc.css('object').each do |node|
        children = node.children
        category = Category.where(category: children.css('author-name').inner_text).first_or_create

        if category.save
          puts "cat ok"
        end

        quote = Quotation.new(quote: children.css('quote').inner_text, author_name: children.css('category').inner_text, category_id: category.id)
        if quote.save
          puts "quote ok"
        end
      end
    end

    @data = []
    quotations = Quotation.joins(:category).select('author_name', 'quote', 'category')
    quotations.each do |quotation|
      @data << quotation.attributes
    end

    respond_to do |format|
      format.html { render :quotations }
      format.xml { send_data @data.to_xml, filename: 'quotations.xml' }
      format.json { send_data @data.to_json, filename: 'quotations.json' }
    end
  end

  private

  def quotation_params
    params.require(:quotation).permit(:author_name, :category_id, :quote)
  end

  def category_params
    params.require(:categories).permit(:category)
  end

  def kill_quote_params
    params.require(:quotation_id).permit(:id)
  end

  def reset_params
    params.require(:reset).permit(:value)
  end

end
