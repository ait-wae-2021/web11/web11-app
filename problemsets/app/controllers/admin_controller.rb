class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show edit update reset destroy ban allow]
  def index
    @admin_role = Role.find_by_name('Admin')
    @teamleader_role = Role.find_by_name('Team Leader')
    @medicalofficer_role = Role.find_by_name('Medical Officer')
    @doctor_role = Role.find_by_name('Doctor')
    @volunteer_role = Role.find_by_name('Volunteer')

    if params[:search]
      user_query = 'lower(email) LIKE?'
      @users = User.where(user_query, "%#{params[:search]}%")
    else
      @users = User.all
    end
  end

  def show; end

  def new
    @user = User.new
  end

  def add_user
    @user = User.new(params[:user].permit(:name, :marital_status, :gender_id, :education, :address, :team, :role_id, :email, :password, :password_confirmation))
    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_index_path, notice: 'User created successfully' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, notice: 'User was failed to create.', status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admin_index_url, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.delete
    respond_to do |format|
      format.html { redirect_to admin_index_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def ban
    @user.is_banned = true
    @user.save
    respond_to do |format|
      format.html { redirect_to admin_index_path, notice: 'User was successfully Banned.' }
      format.json { head :no_content }
    end
  end

  def allow
    @user.is_banned = false
    @user.save
    respond_to do |format|
      format.html { redirect_to admin_index_path, notice: 'User was successfully Allowed.' }
      format.json { head :no_content }
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :marital_status, :gender_id, :education, :address, :team, :role_id, :email)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
