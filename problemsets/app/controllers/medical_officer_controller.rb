class MedicalOfficerController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show]
  def index
    role_id = Role.find_by_name('Doctor').id
    if params[:search]
      user_query = "role_id = #{role_id} AND lower(email) LIKE?"
      @users = User.where(user_query, "%#{params[:search]}%")
    else
      user_query = "role_id = #{role_id}"
      @users = User.where(user_query)
    end
  end

  def show; end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
