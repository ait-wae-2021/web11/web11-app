# frozen_string_literal: true

# Class for Session Controller
class SessionController < ApplicationController
  before_action :authenticate_user!
  def index
    path = case current_user.role.name
           when 'Admin'
             admin_index_path
           when 'Doctor'
             doctor_index_path
           when 'Reception'
             reception_index_path
           when 'Team Leader'
             team_leader_index_path
           when 'Medical Officer'
             medical_officer_index_path
             # when 'Employee'
             #   employee_index_path
             # when 'Volunteer'
             #   volunteer_index_path
           end
    redirect_to path
  end
end
