class ReceptionController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show]

  def index
    doc_role_id = Role.find_by_name('Doctor').id
    if params[:search_doctor]
      user_query = "role_id = #{doc_role_id} AND lower(email) LIKE?"
      @doctors = User.where(user_query, "%#{params[:search]}%")
    else
      user_query = "role_id = #{doc_role_id}"
      @doctors = User.where(user_query)
    end

    med_role_id = Role.find_by_name('Medical Officer').id
    if params[:search_medical_officer]
      user_query = "role_id = #{med_role_id} AND lower(email) LIKE?"
      @medicalofficers = User.where(user_query, "%#{params[:search]}%")
    else
      user_query = "role_id = #{med_role_id}"
      @medicalofficers = User.where(user_query)
    end
  end

  def show; end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
