class TeamLeaderController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show assign update]
  def index
    if params[:search]
      user_query = 'lower(email) LIKE?'
      @users = User.where(user_query, "%#{params[:search]}%")
    else
      @users = User.all
    end
  end

  def show; end

  def assign; end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to team_leader_index_path, notice: 'User was assigned team Successfully' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:team)
  end
end
