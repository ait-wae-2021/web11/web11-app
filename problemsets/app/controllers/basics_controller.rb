class BasicsController < ApplicationController
  before_action :set_basic, only: %i[ show edit update destroy ]
  require 'open-uri'
  require 'nokogiri'

  # GET /basics or /basics.json
  def index
    @basics = Basic.all
    get_headlines
  end

  # GET /basics/1 or /basics/1.json
  def show
  end

  # GET /basics/new
  def new
    @basic = Basic.new
  end

  # GET /basics/1/edit
  def edit
  end

  # POST /basics or /basics.json
  def create
    @basic = Basic.new(basic_params)

    respond_to do |format|
      if @basic.save
        format.html { redirect_to @basic, notice: "Basic was successfully created." }
        format.json { render :show, status: :created, location: @basic }
      else
        # format.html { render :new, status: :unprocessable_entity }
        # format.json { render json: @basic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /basics/1 or /basics/1.json
  def update
    respond_to do |format|
      if @basic.update(basic_params)
        format.html { redirect_to @basic, notice: "Basic was successfully updated." }
        format.json { render :show, status: :ok, location: @basic }
      else
        # format.html { render :edit, status: :unprocessable_entity }
        # format.json { render json: @basic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basics/1 or /basics/1.json
  def destroy
    @basic.destroy
    respond_to do |format|
      format.html { redirect_to basics_url, notice: "Basic was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def get_headlines
    arr1 = []
    arr2 = []
    hash = {}
    headline = ''

    arr1.include?(headline: 'https://www.nytimes.com/international/')
    doc = Nokogiri::HTML(URI.open('https://www.nytimes.com/'))
    doc.xpath('//section[@class="story-wrapper"]//div//a/@href').each do |link|
      unless !link.content.include?('wirecutter') && !link.content.include?('cooking') && !link.content.include?('puzzles') && !link.content.include?('subscription') && !link.content.include?('crosswords') && !hash.key?(link.content)
        next
      end

      hash[link.content] = 1
      url = "//a[@href=\"#{link.content}\"]//h3"
      doc.search(url).each do |link2|
        headline = link2.content
      end
      unless hash.key?(headline)
        arr1.push(Basic.new(headline: headline, link: link.content))
        hash[headline] = 1
      end
    end
    @basics1 = arr1

    hash = {}
    doc = Nokogiri::HTML(URI.open('https://www.bangkokpost.com/world'))
    doc.xpath('//div[@class="section-update section-news--highlight"]//a', '//div[@class="section-recommend"]//a',
              '//div[@class="section-latest"]//h3//a', '//div[@class="section-mostviewed"]//a').each do |link|
      link_edit = "https://www.bangkokpost.com#{link['href']}"
      unless link.content.size > 6 && !Basic.exists?(headline: link.content) && !link.content.include?('view_comment=1') && !hash.key?(link.content)
        next
      end
      arr2.push(Basic.new(headline: link.text.strip, link: link_edit))
      hash[link.content] = 1
    end
    @basics2 = arr2
  end

  # Divide By Zero Error Logging
  def divide_by_zero
    logger.debug 'About to divide by 0'
    10 / 0
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic
      @basic = Basic.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def basic_params
      params.require(:basic).permit(:headline)
    end
end
