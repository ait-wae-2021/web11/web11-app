class DoctorController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show]

  def index; end

  def show; end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
