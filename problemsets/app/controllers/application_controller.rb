class ApplicationController < ActionController::Base
  before_action :banned?
  def after_sign_in_path_for(resource)
    if resource.is_a?(User) && resource.is_banned?
      sign_out resource
      flash[:error] = 'This account has been banned by the administrator.'
      root_path
    else
      session_index_path(current_user)
    end
  end

  def banned?
    if current_user.present? && current_user.is_banned?
      sign_out current_user
      flash[:error] = 'This account has been banned by the administrator.'
      root_path
    end
  end
end
