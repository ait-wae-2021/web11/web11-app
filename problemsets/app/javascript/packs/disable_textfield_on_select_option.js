if (document.getElementById('quotation_category_id').value != 0) {
    document.getElementById('category_field').disabled = true;
}
else {
    document.getElementById('category_field').disabled = false;
}

document.getElementById('quotation_category_id').addEventListener('change', function () {
    if (document.getElementById('quotation_category_id').value != 0) {
        document.getElementById('category_field').disabled = true;
    }
    else {
        document.getElementById('category_field').disabled = false;
    }
});