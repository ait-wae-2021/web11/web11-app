set :default_env, {'http_proxy'=>'192.41.170.23:3128',
                   'https_proxy'=>'192.41.170.23:3128'}

server "web11", user: "deploy", roles: %w{app db web}

set :deploy_to, "/home/deploy/studentdb"

set :yarn_flags, '--production --silent --no-progress --network-timeout 1000000'