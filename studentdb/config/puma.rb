# local config
# max_threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
# min_threads_count = ENV.fetch("RAILS_MIN_THREADS") { max_threads_count }
#
# threads min_threads_count, max_threads_count
#
# worker_timeout 3600 if ENV.fetch("RAILS_ENV", "development") == "development"
# port ENV.fetch("PORT") { 3000 }
#
# # Specifies the `environment` that Puma will run in.
# #
# environment ENV.fetch("RAILS_ENV") { "development" }
#
# # Specifies the `pidfile` that Puma will use.
# pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }
#
# plugin :tmp_restart


# Production config
rails_env = ENV.fetch("RAILS_ENV") { "production" }
environment rails_env

# If production mode, use two workers with preloaded application

if rails_env == "production"
  ENV['https_proxy'] = 'http://192.41.170.23:3128'
  ENV['http_proxy'] = 'http://192.41.170.23:3128'
  workers 2
  preload_app!
  # Location of the control socket. Must be created in advance.
  app_dir = File.expand_path("../..", __FILE__)
  bind "unix://#{app_dir}/tmp/sockets/puma.sock"
end

# 1-5 threads per worker

threads 1, 5

# Run on port 3000 by default

port ENV.fetch("PORT") { 3000 }

# Specify the server's PID file

pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }

# Allow puma to be restarted by the "rails restart" command

plugin :tmp_restart

# Activate the control app

activate_control_app